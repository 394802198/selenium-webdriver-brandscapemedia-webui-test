package com.dxdg.pageobjects;

import com.dxdg.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Steven on 2015/5/24.
 */
public class HomePage {

    private boolean homePageExist;

    public WhyCup clickWhyCupLinkage(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.linkText("Why cup"));

        WebUtil.click(driver, By.linkText("Why cup"));

        return PageFactory.initElements(driver, WhyCup.class);
    }

    public boolean isHomePageExist(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.id("carousel_img1"));

        return WebUtil.isElementExist(driver, By.id("carousel_img1"));
    }
}
