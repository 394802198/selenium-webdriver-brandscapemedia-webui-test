package com.dxdg.pageobjects;

import com.dxdg.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Steven on 2015/5/24.
 */
public class ContactUs {

    public void fillContactName(WebDriver driver, String contactName) {
        WebUtil.waitForElementVisible(driver, By.cssSelector("input[data-name='name']"));

        WebUtil.clearAndSendKeys(driver, By.cssSelector("input[data-name='name']"), contactName);
    }

    public void fillContactEmail(WebDriver driver, String contactEmail) {
        WebUtil.clearAndSendKeys(driver, By.cssSelector("input[data-name='email_address']"), contactEmail);
    }

    public void fillContactPhone(WebDriver driver, String contactPhone) {
        WebUtil.clearAndSendKeys(driver, By.cssSelector("input[data-name='phone_number']"), contactPhone);
    }

    public void fillContactMessage(WebDriver driver, String contactMessage) {
        WebUtil.clearAndSendKeys(driver, By.cssSelector("textarea[data-name='message']"), contactMessage);
    }

    public void clickSendMessageButton(WebDriver driver) {
        WebUtil.click(driver, By.cssSelector("a[data-name='send_message']"));
    }

    public boolean isSuccessMessageExist(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.id("text-error"));

        return WebUtil.isElementDisplayed(driver, By.id("text-error"));
    }

    public HomePage goHomePage(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.cssSelector("a[href='home']"));

        WebUtil.click(driver, By.cssSelector("a[href='home']"));

        return PageFactory.initElements(driver, HomePage.class);
    }
}
