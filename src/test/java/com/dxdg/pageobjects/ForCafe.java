package com.dxdg.pageobjects;

import com.dxdg.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Steven on 2015/5/24.
 */
public class ForCafe {

    public QAndA clickQAndALinkage(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.linkText("Q&A"));

        WebUtil.click(driver, By.linkText("Q&A"));

        return PageFactory.initElements(driver, QAndA.class);
    }
}
