package com.dxdg.pageobjects;

import com.dxdg.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Steven on 2015/5/24.
 */
public class WhyCup {

    public ForCafe clickForCafeLinkage(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.linkText("For cafe"));

        WebUtil.click(driver, By.linkText("For cafe"));

        return PageFactory.initElements(driver, ForCafe.class);
    }
}
