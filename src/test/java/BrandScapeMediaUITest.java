import com.dxdg.pageobjects.*;
import com.dxdg.util.WebUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by Steven on 2015/5/24.
 */
public class BrandScapeMediaUITest {

    WebDriver driver;

    @Before
    public void setDriver(){
        String browserName = System.getenv("browser");
        if(browserName!=null && browserName.equalsIgnoreCase("Chrome")){
            driver = new ChromeDriver();
        } else {
            driver = new FirefoxDriver();
        }
    }

    @Test
    public void testAllPages(){
        // 1. Go Home Page
        HomePage homePage = WebUtil.goHomePage(driver);
        // 2. Click Why Cup
        WhyCup whyCup = homePage.clickWhyCupLinkage(driver);
        // 3. Click For Cafe
        ForCafe forCafe = whyCup.clickForCafeLinkage(driver);
        // 4.1 Click Q&A
        QAndA qAndA = forCafe.clickQAndALinkage(driver);
        // 4.2 First Q&A
        qAndA.expandQAndAByPartialLinkage(driver, "Q: Where did you print and make these cups?");
        // 4.3 First Q&A
        qAndA.expandQAndAByPartialLinkage(driver, "Q: What sorts of inks are used on your products to ensure they are non-toxic?");
        // 4.4 First Q&A
        qAndA.expandQAndAByPartialLinkage(driver, "Q: Can I choose which caf");
        // 4.5 First Q&A
        qAndA.expandQAndAByPartialLinkage(driver, "Q: What's your lead time");
        // 5.1 Click Contact Us
        ContactUs contactUs = qAndA.clickContactUsLinkage(driver);
        // Prepare data
        final String contactName = "Selenium";
        final String contactEmail = "Selenium@gmail";
        final String contactPhone = "0987654321";
        final String contactMessage = "Hi This is Selenium Automation Test";
        // 5.2 Fill Contact Name
        contactUs.fillContactName(driver, contactName);
        // 5.3 Fill Contact Email
        contactUs.fillContactEmail(driver, contactEmail);
        // 5.4 Fill Contact Phone
        contactUs.fillContactPhone(driver, contactPhone);
        // 5.5 Fill Contact Message
        contactUs.fillContactMessage(driver, contactMessage);
        // 5.6 Click Send Message Button
        contactUs.clickSendMessageButton(driver);
        // 6. Verify if message sent successful
        Assert.assertTrue("Successful Message should be exist", contactUs.isSuccessMessageExist(driver));
        // 7. Go Back Home
        homePage = contactUs.goHomePage(driver);
        // 8. Verify if home page exist
        Assert.assertTrue("Home Page should be exist", homePage.isHomePageExist(driver));
    }


    @After
    public void tearDown(){
        driver.quit();
    }
}
